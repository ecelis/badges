QuickBadges
===========

Create badges from templates with pictures taken by the webcam all from the web application.



1. Develop
----------

  1. git clone git@bitbucket.org:ecelis/badges.git
  2. git branch myuser ; git checkout myuser
  3. In Eclipse File->Import->Existing Maven Project
  4. Right click on project and select Run As... -> Maven build
    * Set clean package as goals
  5. Right click on project and select Run As... -> Java Application
    * Use net.celisdelafuente.badges.Main as Main class
  6. Visit http://localhost:8080 in your web browser


2. API
------

  * http://www.example.com/rest/v1


#### User Resource

##### GET

###### /user

###### /user/{id}

##### POST

###### /user

####### name


#### Orgnization Resource


#### Organizational Unit Resource



---
Copyright 2013, 2014 Ernesto Angel Celis de la Fuente
The code of Badges is released under the Apache 2.0 License terms.
