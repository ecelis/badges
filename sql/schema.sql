---
--- Licensed to you under one or more contributor license agreements.  
--- See the NOTICE file distributed with this work for additional information
--- regarding copyright ownership.  
--- Ernesto Angel Celis de la Fuente licenses this file to you under the 
--- Apache License, Version 2.0 (the "License"); you may not use this file except
--- in compliance with the License.  You may obtain a copy of the License at
---
---   http://www.apache.org/licenses/LICENSE-2.0
---
--- Unless required by applicable law or agreed to in writing,
--- software distributed under the License is distributed on an
--- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
--- KIND, either express or implied.  See the License for the
--- specific language governing permissions and limitations
--- under the License.
---

CREATE TABLE member_types (
    id bigint NOT NULL,
    description text
);

CREATE TABLE org_membership (
    id bigint NOT NULL,
    last_name text,
    mother_name text,
    name text,
    org_unit_id bigint,
    org_membership_type bigint,
    creation_date date
);

CREATE TABLE org_units (
    id bigint NOT NULL,
    name text,
    tagline text
);

CREATE TABLE organizations (
    id bigint NOT NULL,
    name text,
    tagline text,
    creation_date date,
    user_id bigint
);

CREATE SEQUENCE seq_member_types
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_org_membership
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_org_units
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_organizations
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE SEQUENCE seq_users
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE users (
    id bigint NOT NULL,
    last_name text,
    mother_name text,
    name text,
    email text,
    password text,
    salt text,
    date_created date
);

ALTER TABLE ONLY users
    ADD CONSTRAINT idx_email UNIQUE (email);

ALTER TABLE ONLY member_types
    ADD CONSTRAINT member_type_pk PRIMARY KEY (id);

ALTER TABLE ONLY organizations
    ADD CONSTRAINT organization_pk PRIMARY KEY (id);

ALTER TABLE ONLY org_membership
    ADD CONSTRAINT org_member_pk PRIMARY KEY (id);

ALTER TABLE ONLY org_units
    ADD CONSTRAINT org_unit_pk PRIMARY KEY (id);

ALTER TABLE ONLY organizations
    ADD CONSTRAINT uiq_organization_name UNIQUE (name);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);

ALTER TABLE ONLY org_membership
    ADD CONSTRAINT member_member_type_fk FOREIGN KEY (org_membership_type) REFERENCES member_types(id);

ALTER TABLE ONLY org_membership
    ADD CONSTRAINT member_org_unit_fk FOREIGN KEY (org_unit_id) REFERENCES org_units(id);

ALTER TABLE ONLY organizations
    ADD CONSTRAINT organization_user_fk FOREIGN KEY (user_id) REFERENCES users(id);
