/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package net.celisdelafuente.badges.data;

import java.sql.SQLException;
import java.util.Properties;

import net.celisdelafuente.badges.beans.OrgUnitBean;
import net.celisdelafuente.badges.beans.OrganizationBean;
import net.celisdelafuente.badges.beans.UserBean;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

public class OrmManager {
	
	private Dao<UserBean, String> userDao;
	private Dao<OrganizationBean, String> organizationDao;
	private Dao<OrgUnitBean, String> orgUnitDao; 

	private String dbUser;
	private String dbPassword;
	private String dbName;
	private String dbHost;
	private String dbPort;
	private String url;
	
	Properties p;
	
	public OrmManager() {
		ConnectionSource connection;
		dbHost = System.getenv("OPENSHIFT_POSTGRESQL_DB_HOST");
		dbUser = System.getenv("OPENSHIFT_POSTGRESQL_DB_USERNAME");
		dbPassword = System.getenv("OPENSHIFT_POSTGRESQL_DB_PASSWORD");
		dbName = "badges";
		if(dbUser == null || dbUser.isEmpty())
			dbUser = "ecelis";
		
		if(dbHost == null || dbHost.isEmpty())
			dbHost = "127.0.0.1";
		
		if(dbPassword == null || dbPassword.isEmpty())
			dbPassword = "123";
		
		url = "jdbc:postgresql://"+dbHost+"/"+dbName+"?user="+dbUser+
				"&password="+dbPassword;		
		
		try {
			connection = new JdbcConnectionSource(url);
			userDao = DaoManager.createDao(connection, UserBean.class);
			organizationDao = DaoManager.createDao(connection, 
					OrganizationBean.class);
			orgUnitDao = DaoManager.createDao(connection, 
					OrgUnitBean.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Dao<UserBean, String> getUserDao() {
		return userDao;
	}
	
	public Dao<OrganizationBean, String> getOrganizationDao() {
		return organizationDao;
	}
	
	public Dao<OrgUnitBean, String> getOrgUnitDao() {
		return orgUnitDao;
	}
}
