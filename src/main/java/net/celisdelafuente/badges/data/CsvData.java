/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package net.celisdelafuente.badges.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import net.celisdelafuente.badges.beans.StudentBean;
import net.celisdelafuente.java.util.FileSystem;

import org.apache.log4j.Logger;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;

public class CsvData implements IData {
	
	private String sourcePath;
	private ArrayList<StudentBean> people = null;
	private Logger logger;
	private FileSystem app = new FileSystem("badges");
	
	public CsvData() {
		logger = getLogger();
		sourcePath = app.getAppHome() + File.separator + "alumnos.csv";
	}

	private Logger getLogger() {
		if(logger == null) {
			logger = Logger.getLogger(getClass());
		}
		return logger;
	}
	
	@Override
	public ArrayList<StudentBean> get() {
		if(people.equals(null)) {
			logger.warn("ArrayList<StudentBean> is null, trying to fetch rows " +
				"from " + sourcePath);
			fetch();
		}
		return people;
	}

	@Override
	public boolean fetch() {
		// CSV fields mapping strategy, by position
		ColumnPositionMappingStrategy<StudentBean> map = 
			new ColumnPositionMappingStrategy<StudentBean>();
		map.setType(StudentBean.class);
		// fields to bind to in the StudentBean
		String[] cols = new String[] {"id","lastName","motherName","name",
			"career","grade","group"};
		map.setColumnMapping(cols);
		CsvToBean<StudentBean> csv = new CsvToBean<StudentBean>();
		// Read the CSV file
		try {
			CSVReader reader = new CSVReader(new FileReader(sourcePath));
			people = (ArrayList<StudentBean>) csv.parse(map, reader);
			return true;
		} catch (FileNotFoundException e) {
			logger.error(sourcePath + " couldn't be found");
		}
		return false;
	}

}
