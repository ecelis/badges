/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package net.celisdelafuente.badges.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import net.celisdelafuente.badges.beans.StudentBean;
import net.celisdelafuente.java.util.FileSystem;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelData implements IData {

	@Override
	public ArrayList<StudentBean> get() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean fetch() {
		// TODO Auto-generated method stub
		String fileName = "alumnos.xls";
		FileSystem fs = new FileSystem("badges");
		Workbook workbook;
		Sheet sheet;
		Iterator<Row> rowIterator;
		try {
			// Workbook file
			FileInputStream file = new FileInputStream(
				new File(fs.getAppHome() + "/"+fileName));
			// Get workbook instance
			switch (fileName.substring(fileName.indexOf(",")+1)) {
			case "xls":
				workbook = new HSSFWorkbook(file);
				// Get first sheet from workbook
				sheet = (HSSFSheet)((Workbook) workbook).getSheetAt(0);
				// Get iterator of all rows in current sheet
				rowIterator = ((HSSFSheet) sheet).iterator();
				break;
			case "xlsx":
				workbook = new XSSFWorkbook(file);
				sheet = (XSSFSheet)((Workbook) workbook).getSheetAt(0);
				rowIterator = ((XSSFSheet) sheet).iterator();
				break;
			default:
				// TODO Throw exception filetype unknown
				break;
			}			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
