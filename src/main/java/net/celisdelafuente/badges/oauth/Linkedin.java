/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package net.celisdelafuente.badges.oauth;

import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.LinkedInApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

public class Linkedin {

	private static final String PROTECTED_RESOURCE_URL = "http://api.linkedin.com/v1/people/~/connections:(id,last-name)";
	private OAuthService service;
	private Token requestToken;
	private Token accessToken;
	private Verifier verifier;
	private OAuthRequest request;
	private Response response;
	
	public Linkedin() {
		service = new ServiceBuilder()
			.provider(LinkedInApi.class)
			.apiKey("")
			.apiSecret("")
			.build();
		requestToken = service.getRequestToken();
	}

	public boolean verify(String value) {
		verifier = new Verifier(value);
		accessToken = service.getAccessToken(requestToken, verifier);
		return true;
	}
}
