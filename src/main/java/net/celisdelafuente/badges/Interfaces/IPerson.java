/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package net.celisdelafuente.badges.Interfaces;

import java.util.ArrayList;

public interface IPerson {
	
	// Getters
	public String getId();
	public String getLastName();
	public String getMotherName();
	public String getName();
	// Setters
	public void setId(String id);
	public void setLastName(String lastName);
	public void setMotherName(String motherName);
	public void setName(String name);
	// persistence
	public boolean save();
	public ArrayList<IPerson> find();
	public IPerson findById(String id);
	
}
