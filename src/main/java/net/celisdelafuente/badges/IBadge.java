/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package net.celisdelafuente.badges;

import java.awt.image.BufferedImage;
import java.io.File;

public interface IBadge {
	
	public void setPath(String p);
	
	public String getPath();
	
	public void setBgImage(File bg);
	
	public File getBgImage();
	
	public BufferedImage getPicture();
	
	public void setFullName(String name);
	
	public String getFullName();
	
	public boolean save();

	void setPicture(BufferedImage pic);
}
