/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package net.celisdelafuente.badges;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.celisdelafuente.badges.beans.StudentBean;
import net.celisdelafuente.java.util.FileSystem;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

public class Badge implements IBadge {
	
	private String path;
	private File bgImage;
	private BufferedImage picture;
	private String fullName;
	private String career;
	private Logger logger;
	private String grade;
	private String group;
	private FileSystem app = new FileSystem("badges");
	
	public Badge() {
		this.onInit();
	}
	
	public Badge(StudentBean person) {
		this.onInit();
		fullName = person.getName() + " "
			+ person.getLastName() + " "
			+ person.getMotherName();
		career = person.getCareer();
		grade = person.getGrade();
		group = person.getGroup();
	}
	
	public void crop(String coords) {
		String[] c = coords.split(",");
		int x,y,w,h;
		w = Integer.parseInt(c[0]);
		h = Integer.parseInt(c[3]);
		x = Integer.parseInt(c[1]);
		y = Integer.parseInt(c[2]);
		picture = picture.getSubimage(x,y,w,h);
		// TODO crop isn't working FIX IT ASAP!
		//picture = picture.getSubimage(x,y,w,h);
	}
	
	@Override
	public File getBgImage() {
		return bgImage;
	}
	
	@Override
	public String getFullName() {
		return fullName;
	}
	
	public String getGrade() {
		return grade;
	}
	
	public String getGroup() {
		return group;
	}
	
	private Logger getLogger() {
		if(logger == null) {
			logger = Logger.getLogger(getClass());
		}
		return logger;
	}
	
	@Override
	public String getPath() { return path; }
	
	@Override
	public BufferedImage getPicture() { return picture; }
	
	public void onInit() {
		logger = getLogger();
		path = app.getAppHome() + File.separator;
		bgImage = new File(path + "bgImage.jpg");
	}
	
	@Override
	public boolean save() {
		// Set font settings
		Font font = new Font("Avenir Next Condesed",Font.BOLD, 30);
		
		// load source images
		BufferedImage image;
		//BufferedImage overlay;
		try {
			image = ImageIO.read(bgImage);
		} catch (IOException e) {
			logger.error("Either image or overlay failed to be set ==>> " 
		+ e.toString());
			return false;
		}
	
		// create the new image, canvas size is the same as bgImage
		int w = image.getWidth();
		int h = image.getHeight();
		//BufferedImage combined = new BufferedImage(w, h, BufferedImage
			//BufferedImage.TYPE_INT_ARGB);
		BufferedImage combined = new BufferedImage (w, h, BufferedImage.OPAQUE);
		
		// paint both images and text, preserving the alpha channels
		Graphics g = combined.getGraphics();
		g.setFont(font);
		g.drawImage(image, 0, 0, null);
		// TODO scale preserving proportion
		g.drawImage(picture, 715, 187, 240, 320, null);
		g.drawString(fullName, 180, 382);
		g.drawString(career, 245, 421);
		g.drawString(grade, 170, 515);
		g.drawString(group, 260, 515);
		// Save as new image
		try {
			ImageIO.write(combined, "jpg", new File(path, 
				grade + "_" + group + "_" + fullName + ".jpg"));} catch (IOException e) {
			logger.error("Couldn't save the new badge " + e.toString());
			//e.printStackTrace();
			return false;
		}
		
		return true;
	}

	@Override
	public void setBgImage(File bg) {
		bgImage = bg;
	}

	@Override
	public void setFullName(String name) {
		fullName = name;		
	}

	public void setGrade(String g){
		grade = g;
	}
	
	public void setGroup(String grp) {
		group = grp;
	}
	
	@Override
	public void setPath(String p) {
		path = p;
	}
	
	@Override
	public void setPicture(BufferedImage pic) {
		picture = pic;
	}
	
	public boolean toJpg(String image) {
		//InputStream in = stream;
		try {
			String base64 = image.substring(image.indexOf(",")+1);
			byte[] imageBytes = Base64.decodeBase64(base64);
			ByteArrayInputStream stream = new ByteArrayInputStream(imageBytes);
			picture = ImageIO.read(stream);
			stream.close();
			if(picture != null) {
				return true;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
}
