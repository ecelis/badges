/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package net.celisdelafuente.badges;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.Configuration;

/**
 * Quick Badges entry point
 *
 */
public class Main
{
private Server appServer;
	
	public static void main(String[] args) {
		Main server = new Main();
		server.start();
	}
	
	public void start() {		
		String host = System.getenv("OPENSHIFT_DIY_IP"); // TODO Read from file
		if(host == null || host.isEmpty())
			host = "0.0.0.0";
		String webroot = "src/main/webapp/";
		appServer = new Server();
		ServerConnector connector = new ServerConnector(appServer);
		connector.setHost(host);
		String port = System.getenv("PORT");
		if(port == null || port.isEmpty())
			port = "5081";
		connector.setPort(Integer.valueOf(port));	// TODO Read from file
		appServer.addConnector(connector);
		WebAppContext root = new WebAppContext();
		root.setContextPath("/");
		root.setDescriptor(webroot + "/WEB-INF/web.xml");
		root.setResourceBase(webroot);
		root.setParentLoaderPriority(true);

		// This webapp will use jsps and jstl. We need to enable the
        // AnnotationConfiguration in order to correctly
        // set up the jsp container
		Configuration.ClassList classlist = Configuration.ClassList
				.setServerDefault(appServer);
		classlist.addBefore("org.eclipse.jetty.webapp.JettyWebXmlConfiguration", 
				"org.eclipse.jetty.annotations.AnnotationConfiguration");
		
		appServer.setHandler(root);
		try {
			appServer.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			appServer.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void stop() {
		try {
			appServer.stop();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
