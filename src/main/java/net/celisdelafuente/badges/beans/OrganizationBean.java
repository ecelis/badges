/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package net.celisdelafuente.badges.beans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.celisdelafuente.badges.data.OrmManager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "organizations")
public class OrganizationBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6550130527040707004L;
	
	private OrmManager om = new OrmManager();
	private Dao dao;
	
	@DatabaseField(generatedIdSequence = "seq_organizations")
	private int id;
	@DatabaseField
	private String name;
	@DatabaseField
	private String tagline;
	/*@DatabaseField
	private int user_id;*/
	@DatabaseField(dataType = DataType.DATE)
	private Date creation_date;
	@DatabaseField(canBeNull = false, foreign = true)
	private UserBean user;

	public OrganizationBean() {
		this.init();
	}
	
	public OrganizationBean(String name, String tagline, UserBean user) {
		this.init();
		this.name = name;
		this.tagline = tagline;
		//this.user_id = user_id;
		this.user = user;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTagline() {
		return tagline;
	}

	public void setTagline(String tagline) {
		this.tagline = tagline;
	}

	public UserBean getUser() {
		return user;
	}

	public void setUser(UserBean user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public Date getCreation_date() {
		return creation_date;
	}

	public List<OrganizationBean> find() {
		List<OrganizationBean> organizations = new ArrayList<>();
		try {
			organizations = dao.queryForAll();
			return organizations;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return organizations;
	}
	
	
	public OrganizationBean findById(int id) {
		Dao<OrganizationBean, String> dao = om.getOrganizationDao();
		OrganizationBean organization = new OrganizationBean();
		try {
			organization = dao.queryForId(String.valueOf(id));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return organization;
			
	}
	
	public boolean save() {
		try {
			this.creation_date = new Date();
			int tmp = dao.create(this);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	private void init() {
		this.dao = om.getOrganizationDao();
	}

}
