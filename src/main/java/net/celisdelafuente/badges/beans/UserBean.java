package net.celisdelafuente.badges.beans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.celisdelafuente.badges.data.OrmManager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "users")
public class UserBean extends PersonBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7245040743151449358L;

	private OrmManager om;
	private Dao<UserBean, String> dao;
	private static UserBean user = new UserBean();
	private List<UserBean> users;

	@DatabaseField(generatedIdSequence = "seq_users")
	private int id;
	@DatabaseField(columnName = "last_name")
	private String lastName;
	@DatabaseField(columnName = "mother_name")
	private String motherName;
	@DatabaseField(columnName = "name")
	private String name;
	@DatabaseField
	private String email;
	@DatabaseField
	private String salt;
	@DatabaseField
	private String password;

	public UserBean() {
		// ORMLITE required
		this.init();
	}

	public UserBean(String lastName, String motherName, String name,
			String email, String salt, String password) {
		this.init();
		user.lastName = lastName;
		user.motherName = motherName;
		user.name = name;
		user.email = email;
		user.password = password;
		user.salt = salt;
	}

	public List<UserBean> find() {
		try {
			users = dao.queryForAll();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return users;
	}

	public UserBean findById(int id) {
		try {
			user = dao.queryForId(String.valueOf(id));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;

	}

	public String getEmail() {
		return email;
	}

	public int getId() {
		return id;
	}

	public String getLastName() {
		return lastName;
	}

	public String getMotherName() {
		return motherName;
	}

	public String getName() {
		return name;
	}
	
	public static UserBean getUserInstance() {
		return user;
	}

	public void init() {
		users = new ArrayList<>();
		om = new OrmManager();
		dao = om.getUserDao();
	}
	
	public boolean login(Map<String, Object> credentials) {
		try {
			users = dao.queryForFieldValues(credentials);
			if(users.size() > 1 || users.size() < 1) {
				// TODO something is wrong
			} else {
				user = users.get(0);
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public int save() {
		try {
			return dao.create(this);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
