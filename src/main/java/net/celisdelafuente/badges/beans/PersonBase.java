/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package net.celisdelafuente.badges.beans;

/**
 * @author ecelis
 *
 */
public abstract class PersonBase {
	
	protected int id;
	protected String lastName;
	protected String motherName;
	protected String name;

	/**
	 * 
	 */
	public PersonBase() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see net.celisdelafuente.badges.Interfaces.IPerson#getId()
	 */
	
	public int getId() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see net.celisdelafuente.badges.Interfaces.IPerson#getLastName()
	 */
	
	public String getLastName() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see net.celisdelafuente.badges.Interfaces.IPerson#getMotherName()
	 */
	
	public String getMotherName() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see net.celisdelafuente.badges.Interfaces.IPerson#getName()
	 */
	
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see net.celisdelafuente.badges.Interfaces.IPerson#setId(java.lang.String)
	 */
	
	public void setId(String id) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see net.celisdelafuente.badges.Interfaces.IPerson#setLastName(java.lang.String)
	 */
	
	public void setLastName(String lastName) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see net.celisdelafuente.badges.Interfaces.IPerson#setMotherName(java.lang.String)
	 */
	
	public void setMotherName(String motherName) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see net.celisdelafuente.badges.Interfaces.IPerson#setName(java.lang.String)
	 */
	
	public void setName(String name) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see net.celisdelafuente.badges.Interfaces.IPerson#save()
	 */
	
	public int save() {
		// TODO Auto-generated method stub
		return 0;
	}


}
