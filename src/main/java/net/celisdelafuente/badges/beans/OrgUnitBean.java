/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package net.celisdelafuente.badges.beans;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import net.celisdelafuente.badges.data.OrmManager;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "org_units")
public class OrgUnitBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8626812211134828678L;
	private OrmManager om = new OrmManager();
	private Dao dao;
	
	@DatabaseField
	private int id;
	@DatabaseField
	private String name;
	@DatabaseField
	private String tagline;
	@DatabaseField(canBeNull = false, foreign = true)
	private OrganizationBean organization;
	
	public OrgUnitBean() {
		
	}
	
	public OrgUnitBean(String name, String tagline, 
			OrganizationBean organization) { 
		this.init();
		this.name = name;
		this.tagline = tagline;
		this.organization = organization;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTagline() {
		return tagline;
	}
	public void setTagline(String tagline) {
		this.tagline = tagline;
	}
	public int getId() {
		return id;
	}
	
	
	public OrganizationBean getOrganization() {
		return organization;
	}

	public void setOrganization(OrganizationBean organization) {
		this.organization = organization;
	}

	public List<OrgUnitBean> find() {
		List<OrgUnitBean> OrgUnits = new ArrayList<>();
		try {
			OrgUnits = dao.queryForAll();
			return OrgUnits;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return OrgUnits;
	}
	
	
	public OrgUnitBean findById(int id) {
		Dao<OrgUnitBean, String> dao = om.getOrgUnitDao();
		OrgUnitBean OrgUnit = new OrgUnitBean();
		try {
			OrgUnit = dao.queryForId(String.valueOf(id));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return OrgUnit;
			
	}
	
	public boolean save() {
		try {
			OrganizationBean organization = new OrganizationBean().findById(1);
			this.organization = organization;
			int tmp = dao.create(this);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private void init() {
		this.dao = om.getOrgUnitDao();
	}

}
