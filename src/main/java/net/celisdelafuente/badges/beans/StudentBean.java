/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package net.celisdelafuente.badges.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class StudentBean extends PersonBase implements Serializable {
	
    /**
	 * Person JavaBean Serial Version UID
	 */
	private static final long serialVersionUID = -8139732225422598388L;
	
	private String career;
	private String grade;
	private String group;
	private ArrayList<StudentBean> people;
	
	/**
     * Default constructor. 
     */
    public StudentBean() {
    	
    }

	public List<UserBean> find() {
        // TODO Auto-generated method stub
			return null;
    }

	public StudentBean findById(int id) {
    	for(StudentBean student : people) {
    		if(id == student.id) {
    			return student;
    		}
    	}
		return null;
    }

	/**
	 * 
	 */
	public String getCareer() {
		return career;
	}

	public String getGrade() {
		return grade;
	}

	public String getGroup() {
		return group;
	}

	public ArrayList<StudentBean> getPeople() {
		return this.people;
	}

	/**
     * @see IPerson#save()
     */
	@Override
	public int save() {
		// TODO Auto-generated method stub
		return 0;
	}
    
    
    /**
	 *  
	 */
	public void setCareer(String career) {
		this.career = career;
	}

    public void setGrade(String grade) {
		this.grade = grade;
	}
	
	public void setGroup(String group) {
		this.group = group;
	}
	
	public void setPeople(ArrayList<StudentBean> p) {
		this.people = p;
	}

}
