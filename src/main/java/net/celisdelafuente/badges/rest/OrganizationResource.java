/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package net.celisdelafuente.badges.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.celisdelafuente.badges.beans.OrganizationBean;
import net.celisdelafuente.badges.beans.UserBean;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

@Path ("organization")
public class OrganizationResource {
	// TODO use logger
	private Logger logger = Logger.getLogger(getClass());
	
	@POST
	public void save(@FormParam("name") String name,
			@FormParam("tagline") String tagline,
			@FormParam("user_id") int user_id) {
		
		//TODO check
		UserBean user = new UserBean().findById(user_id);
		OrganizationBean organization = 
				new OrganizationBean(name, tagline, user);
		organization.save();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getOrganizations() {
		OrganizationBean o = new OrganizationBean();
		List<OrganizationBean> organizations = o.find();
		String jsonOrganizations = "";
		if(organizations.size() > 0) {
			final OutputStream out = new ByteArrayOutputStream();
			final ObjectMapper mapper = new ObjectMapper();
			try {
				mapper.writeValue(out, organizations);
				final byte[] data = ((ByteArrayOutputStream) out).toByteArray();
				jsonOrganizations = new String(data);
			} catch(IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return "data:" + jsonOrganizations;
	}
	
	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getOrganization(@PathParam("id") int id) {
		OrganizationBean o = new OrganizationBean();
		o = o.findById(id);
		String jsonOrganization = "";
		if(!o.equals(null)) {
			final OutputStream out = new ByteArrayOutputStream();
			final ObjectMapper mapper = new ObjectMapper();
			try {
				mapper.writeValue(out, o);
				final byte[] data = ((ByteArrayOutputStream) out).toByteArray();
				jsonOrganization = new String(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return "data:" + jsonOrganization;
	}

}
