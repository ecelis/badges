/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package net.celisdelafuente.badges.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.celisdelafuente.badges.beans.UserBean;
import net.celisdelafuente.java.util.JSON;

import org.codehaus.jackson.map.ObjectMapper;

import com.google.gson.Gson;

@Path("user")
public class UserResource {

	private UserBean user;
	private List<UserBean> users;
	private String jsonData;
	private String success;
	private String errorMsg;
	private Gson gson = new Gson(); 
	
	public UserResource() {
		// TODO Auto-generated constructor stub
		user = UserBean.getUserInstance();
		jsonData = ""; 
		success = "true";
		errorMsg = "";
	}
	
	@Path("signup")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public void save(@FormParam("name") String name,
			@FormParam("lastName") String lastName,
			@FormParam("motherName") String motherName,
			@FormParam("email") String email,
			@FormParam("password") String password) {
		
			// TODO Java Crypto thingy
			String salt = "";	// TODO Initialize with some land & random value
			UserBean user = new UserBean(lastName, motherName, name, email,
					salt, password);
			user.save();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getUsers() {
		users = user.find();
		if (users.size() > 0) {
			final OutputStream out = new ByteArrayOutputStream();
			final ObjectMapper mapper = new ObjectMapper();
			try {
				mapper.writeValue(out, users);
				final byte[] data = ((ByteArrayOutputStream) out).toByteArray();
				if(data != null) {
					jsonData = new String(data);
				} else {
					success = "false";
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		JSON j = new JSON();
		return j.wrapJson(jsonData, success, errorMsg);
	}
	

	@Path("{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getUser(@PathParam("id") int id) {
		user.findById(id);
		if (!user.equals(null)) {
			final OutputStream out = new ByteArrayOutputStream();
			final ObjectMapper mapper = new ObjectMapper();
			try {
				user = user.getUserInstance();
				mapper.writeValue(out, user);
				final byte[] data = ((ByteArrayOutputStream) out).toByteArray();
				jsonData = new String(data);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return "\"data\":" + jsonData;
	}
	
	@Path("login")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String userLogin(@FormParam("email") String email,
			@FormParam("password") String password) {
		Map<String, Object> fieldValues = new HashMap<String, Object>();
		fieldValues.put("email", email);
		fieldValues.put("password", password);
		if(user.login(fieldValues)) {
			final OutputStream out = new ByteArrayOutputStream();
			final ObjectMapper mapper = new ObjectMapper();
			try {
				mapper.writeValue(out, user.getUserInstance());
				final byte[] data = ((ByteArrayOutputStream) out).toByteArray();
				jsonData = new String(data);
			} catch (IOException e) {
				// TODO handle exception
			}
		} else {
			// TODO something in failed login
		}
		JSON j = new JSON();
		return j.wrapJson(jsonData, "success", "");
	}
}
