/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/
package net.celisdelafuente.badges.rest;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.celisdelafuente.badges.beans.OrgUnitBean;
import net.celisdelafuente.badges.beans.OrganizationBean;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

@Path("orgunit")
public class OrgUnitResource {

	// TODO use logger
		private Logger logger = Logger.getLogger(getClass());
		
		@POST
		public void save(@FormParam("name") String name,
				@FormParam("tagline") String tagline,
				@FormParam("organization_id") int organization_id) {
			
			OrganizationBean organization = new OrganizationBean()
												.findById(organization_id);
			OrgUnitBean OrgUnit = 
					new OrgUnitBean(name, tagline, organization);
			OrgUnit.save();
		}
		
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public String getOrgUnits() {
			OrgUnitBean o = new OrgUnitBean();
			List<OrgUnitBean> OrgUnits = o.find();
			String jsonOrgUnits = "";
			if(OrgUnits.size() > 0) {
				final OutputStream out = new ByteArrayOutputStream();
				final ObjectMapper mapper = new ObjectMapper();
				try {
					mapper.writeValue(out, OrgUnits);
					final byte[] data = ((ByteArrayOutputStream) out).toByteArray();
					jsonOrgUnits = new String(data);
				} catch(IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return "data:" + jsonOrgUnits;
		}
		
		@Path("{id}")
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public String getOrgUnit(@PathParam("id") int id) {
			OrgUnitBean o = new OrgUnitBean();
			o = o.findById(id);
			String jsonOrgUnit = "";
			if(!o.equals(null)) {
				final OutputStream out = new ByteArrayOutputStream();
				final ObjectMapper mapper = new ObjectMapper();
				try {
					mapper.writeValue(out, o);
					final byte[] data = ((ByteArrayOutputStream) out).toByteArray();
					jsonOrgUnit = new String(data);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return "data:" + jsonOrgUnit;
		}
}
