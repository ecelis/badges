ALTER TABLE org_units ADD organization_id int8 NOT NULL;
ALTER TABLE org_units ADD CONSTRAINT fk_org_units_organization FOREIGN KEY (organization_id) REFERENCES organizations(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
