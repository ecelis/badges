CREATE TABLE org_units (
	id int8 NOT NULL,
	name text,
	tagline text
);
ALTER TABLE org_units ADD CONSTRAINT org_unit_pk PRIMARY KEY(id);
CREATE TABLE member_types (
	id int8 NOT NULL,
	description text
);
ALTER TABLE member_types ADD CONSTRAINT member_type_pk PRIMARY KEY(id);
CREATE TABLE org_membership (
	id int8 NOT NULL,
	last_name text,
	mother_name text,
	name text,
	org_unit_id int8,
	org_membership_type int8,
	creation_date date
);
ALTER TABLE org_membership ADD CONSTRAINT org_member_pk PRIMARY KEY(id);
CREATE TABLE users (
	id int8 NOT NULL,
	last_name text,
	mother_name text,
	name text,
	email text,
	password text,
	salt text,
	date_created date
);
ALTER TABLE users ADD CONSTRAINT users_pk PRIMARY KEY(id);
CREATE UNIQUE INDEX idx_email ON users (email);
CREATE TABLE organizations (
	id int8 NOT NULL,
	name text,
	tagline text,
	creation_date date,
	user_id int8
);
ALTER TABLE organizations ADD CONSTRAINT organization_pk PRIMARY KEY(id);
CREATE UNIQUE INDEX uiq_organization_name ON organizations (name);
ALTER TABLE org_membership ADD CONSTRAINT member_member_type_fk FOREIGN KEY (org_membership_type) REFERENCES member_types(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE org_membership ADD CONSTRAINT member_org_unit_fk FOREIGN KEY (org_unit_id) REFERENCES org_units(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE organizations ADD CONSTRAINT organization_user_fk FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE NO ACTION ON UPDATE NO ACTION;
